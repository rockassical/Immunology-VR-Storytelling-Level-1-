<Q                         DIRECTIONAL    FOG_EXP2   STEREO_MULTIVIEW_ON     <   #ifdef VERTEX
#version 300 es
#extension GL_OVR_multiview2 : require

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 unity_FogParams;
uniform 	mediump float _Rotation;
uniform 	mediump vec3 _FrontTopColor;
uniform 	mediump vec3 _FrontBottomColor;
uniform 	mediump vec3 _BackTopColor;
uniform 	mediump vec3 _BackBottomColor;
uniform 	mediump vec3 _RightTopColor;
uniform 	mediump vec3 _RightBottomColor;
uniform 	mediump vec3 _LeftTopColor;
uniform 	mediump vec3 _LeftBottomColor;
uniform 	mediump vec3 _TopColor;
uniform 	mediump vec3 _BottomColor;
uniform 	mediump float _GradientYStartPos;
uniform 	mediump float _GradientHeight;
uniform 	mediump vec3 _AmbientColor;
uniform 	mediump float _AmbientPower;
uniform 	vec4 _MainTex_ST;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityStereoGlobals {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixInvV[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoMatrixVP[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraInvProjection[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoWorldToCamera[8];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_StereoCameraToWorld[8];
	UNITY_UNIFORM vec3 unity_StereoWorldSpaceCameraPos[2];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset[2];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
layout(num_views = 2) in;
in mediump vec4 in_POSITION0;
in mediump vec3 in_NORMAL0;
in mediump vec3 in_COLOR0;
in mediump vec4 in_TEXCOORD0;
out mediump vec2 vs_TEXCOORD0;
out highp float vs_TEXCOORD3;
out mediump vec3 vs_TEXCOORD1;
out mediump vec3 vs_TEXCOORD2;
out highp vec2 vs_TEXCOORD6;
vec4 u_xlat0;
bvec3 u_xlatb0;
vec3 u_xlat1;
int u_xlati1;
vec4 u_xlat2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec4 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump float u_xlat16_27;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlati1 = int(int(gl_ViewID_OVR) << 2);
    u_xlat2 = u_xlat0.yyyy * hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 1)];
    u_xlat2 = hlslcc_mtx4x4unity_StereoMatrixVP[u_xlati1] * u_xlat0.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 2)] * u_xlat0.zzzz + u_xlat2;
    u_xlat0 = hlslcc_mtx4x4unity_StereoMatrixVP[(u_xlati1 + 3)] * u_xlat0.wwww + u_xlat2;
    gl_Position = u_xlat0;
    u_xlat0.x = u_xlat0.z * unity_FogParams.x;
    u_xlat0.x = u_xlat0.x * (-u_xlat0.x);
    vs_TEXCOORD3 = exp2(u_xlat0.x);
    vs_TEXCOORD0.xy = vec2(0.0, 0.0);
    u_xlat16_3.xyz = in_NORMAL0.zxy;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_3.xyz = min(max(u_xlat16_3.xyz, 0.0), 1.0);
#else
    u_xlat16_3.xyz = clamp(u_xlat16_3.xyz, 0.0, 1.0);
#endif
    u_xlat0.xyz = u_xlat16_3.xyz * vec3(-0.0187292993, -0.0187292993, -0.0187292993) + vec3(0.0742610022, 0.0742610022, 0.0742610022);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_3.xyz + vec3(-0.212114394, -0.212114394, -0.212114394);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_3.xyz + vec3(1.57072878, 1.57072878, 1.57072878);
    u_xlat1.xyz = (-u_xlat16_3.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat1.xyz = sqrt(u_xlat1.xyz);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat16_3.xyz = (-u_xlat0.xyz) * vec3(0.636619747, 0.636619747, 0.636619747) + vec3(1.0, 1.0, 1.0);
    u_xlat16_4.xyz = vec3(_FrontTopColor.x, _FrontTopColor.y, _FrontTopColor.z) + (-_FrontBottomColor.xyz);
    u_xlat16_5.x = sin(_Rotation);
    u_xlat16_6.x = cos(_Rotation);
    u_xlat16_27 = u_xlat16_6.x * in_POSITION0.y;
    u_xlat16_27 = in_POSITION0.x * u_xlat16_5.x + u_xlat16_27;
    u_xlat16_27 = u_xlat16_27 + (-_GradientYStartPos);
    u_xlat16_27 = u_xlat16_27 / _GradientHeight;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_27 = min(max(u_xlat16_27, 0.0), 1.0);
#else
    u_xlat16_27 = clamp(u_xlat16_27, 0.0, 1.0);
#endif
    u_xlat16_4.xyz = vec3(u_xlat16_27) * u_xlat16_4.xyz + _FrontBottomColor.xyz;
    u_xlat16_4.xyz = u_xlat16_3.xxx * u_xlat16_4.xyz;
    u_xlat16_5.xyz = _BackTopColor.xyz + (-_BackBottomColor.xyz);
    u_xlat16_5.xyz = vec3(u_xlat16_27) * u_xlat16_5.xyz + _BackBottomColor.xyz;
    u_xlat16_6.xyz = (-in_NORMAL0.zxy);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_6.xyz = min(max(u_xlat16_6.xyz, 0.0), 1.0);
#else
    u_xlat16_6.xyz = clamp(u_xlat16_6.xyz, 0.0, 1.0);
#endif
    u_xlat0.xyz = u_xlat16_6.xyz * vec3(-0.0187292993, -0.0187292993, -0.0187292993) + vec3(0.0742610022, 0.0742610022, 0.0742610022);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_6.xyz + vec3(-0.212114394, -0.212114394, -0.212114394);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_6.xyz + vec3(1.57072878, 1.57072878, 1.57072878);
    u_xlat1.xyz = (-u_xlat16_6.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat1.xyz = sqrt(u_xlat1.xyz);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat16_6.xyz = (-u_xlat0.xyz) * vec3(0.636619747, 0.636619747, 0.636619747) + vec3(1.0, 1.0, 1.0);
    u_xlat16_5.xyz = u_xlat16_5.xyz * u_xlat16_6.xxx;
    u_xlatb0.xyz = lessThan(vec4(0.0, 0.0, 0.0, 0.0), in_NORMAL0.zxyz).xyz;
    u_xlat16_4.xyz = (u_xlatb0.x) ? u_xlat16_4.xyz : u_xlat16_5.xyz;
    u_xlat16_5.xyz = _RightTopColor.xyz + (-_RightBottomColor.xyz);
    u_xlat16_5.xyz = vec3(u_xlat16_27) * u_xlat16_5.xyz + _RightBottomColor.xyz;
    u_xlat16_5.xyz = u_xlat16_3.yyy * u_xlat16_5.xyz;
    u_xlat16_3.xyz = u_xlat16_3.zzz * _TopColor.xyz;
    u_xlat16_7.xyz = _LeftTopColor.xyz + (-_LeftBottomColor.xyz);
    u_xlat16_7.xyz = vec3(u_xlat16_27) * u_xlat16_7.xyz + _LeftBottomColor.xyz;
    u_xlat16_6.xyw = u_xlat16_6.yyy * u_xlat16_7.xyz;
    u_xlat16_7.xyz = u_xlat16_6.zzz * _BottomColor.xyz;
    u_xlat16_3.xyz = (u_xlatb0.z) ? u_xlat16_3.xyz : u_xlat16_7.xyz;
    u_xlat16_5.xyz = (u_xlatb0.y) ? u_xlat16_5.xyz : u_xlat16_6.xyw;
    u_xlat16_4.xyz = u_xlat16_4.xyz + u_xlat16_5.xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz + u_xlat16_4.xyz;
    vs_TEXCOORD1.xyz = vec3(_AmbientColor.x, _AmbientColor.y, _AmbientColor.z) * vec3(_AmbientPower) + u_xlat16_3.xyz;
    vs_TEXCOORD2.xyz = in_COLOR0.xyz;
    vs_TEXCOORD6.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 unity_FogColor;
uniform 	mediump vec3 _LightTint;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
in highp float vs_TEXCOORD3;
in mediump vec3 vs_TEXCOORD1;
in mediump vec3 vs_TEXCOORD2;
in highp vec2 vs_TEXCOORD6;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec3 u_xlat16_0;
mediump vec3 u_xlat16_1;
float u_xlat6;
void main()
{
    u_xlat16_0.xyz = texture(_MainTex, vs_TEXCOORD6.xy).xyz;
    u_xlat16_0.xyz = u_xlat16_0.xyz * vs_TEXCOORD2.xyz;
    u_xlat16_1.xyz = vs_TEXCOORD1.xyz * vec3(_LightTint.x, _LightTint.y, _LightTint.z);
    u_xlat16_0.xyz = u_xlat16_1.xyz * u_xlat16_0.xyz + (-unity_FogColor.xyz);
    u_xlat6 = vs_TEXCOORD3;
#ifdef UNITY_ADRENO_ES3
    u_xlat6 = min(max(u_xlat6, 0.0), 1.0);
#else
    u_xlat6 = clamp(u_xlat6, 0.0, 1.0);
#endif
    u_xlat0.xyz = vec3(u_xlat6) * u_xlat16_0.xyz + unity_FogColor.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
                             $Globals         unity_FogColor                        
   _LightTint                              $Globals         unity_FogParams                   @   	   _Rotation                     P      _FrontTopColor                    T      _FrontBottomColor                     `      _BackTopColor                     t      _BackBottomColor                  �      _RightTopColor                    �      _RightBottomColor                     �      _LeftTopColor                     �      _LeftBottomColor                  �   	   _TopColor                     �      _BottomColor                  �      _GradientYStartPos                    �      _GradientHeight                   �      _AmbientColor                     �      _AmbientPower                           _MainTex_ST                        unity_ObjectToWorld                             UnityStereoGlobals  @  
      unity_StereoWorldSpaceCameraPos                        unity_StereoScaleOffset                        unity_StereoMatrixP                        unity_StereoMatrixV                 �      unity_StereoMatrixInvV                        unity_StereoMatrixVP                �     unity_StereoCameraProjection                      unity_StereoCameraInvProjection                 �     unity_StereoWorldToCamera                         unity_StereoCameraToWorld                   �            _MainTex                  UnityStereoGlobals             