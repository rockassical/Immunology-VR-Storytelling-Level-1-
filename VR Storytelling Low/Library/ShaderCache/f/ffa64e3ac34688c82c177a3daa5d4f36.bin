<Q                         DR_CEL_EXTRA_ON 	   DR_RIM_ON      FOG_EXP2   SHADOWS_DEPTH      SHADOWS_SOFT   SPOT   _CELPRIMARYMODE_STEPS      _TEXTUREBLENDINGMODE_MULTIPLY       E2  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 unity_FogParams;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _BumpMap_ST;
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TEXCOORD0;
out highp vec4 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp float vs_TEXCOORD6;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD4;
out highp vec4 vs_TEXCOORD5;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
float u_xlat10;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    gl_Position = u_xlat1;
    u_xlat1.x = u_xlat1.z * unity_FogParams.x;
    u_xlat1.x = u_xlat1.x * (-u_xlat1.x);
    vs_TEXCOORD6 = exp2(u_xlat1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD0.zw = in_TEXCOORD0.xy * _BumpMap_ST.xy + _BumpMap_ST.zw;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat10 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat10 = inversesqrt(u_xlat10);
    vs_TEXCOORD1.xyz = vec3(u_xlat10) * u_xlat1.xyz;
    u_xlat1.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat1.xyz;
    vs_TEXCOORD2.xyz = u_xlat1.xyz;
    vs_TEXCOORD3.xyz = (-u_xlat1.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD4 = hlslcc_mtx4x4unity_WorldToLight[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD5 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 _ShadowOffsets[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	mediump vec4 _Color;
uniform 	mediump vec4 _FlatRimColor;
uniform 	float _FlatRimSize;
uniform 	float _FlatRimEdgeSmoothness;
uniform 	float _FlatRimLightAlign;
uniform 	mediump float _SelfShadingSizeExtra;
uniform 	mediump float _ShadowEdgeSizeExtra;
uniform 	mediump float _FlatnessExtra;
uniform 	mediump vec4 _ColorDimSteps;
uniform 	mediump vec4 _ColorDimExtra;
uniform 	mediump float _TextureImpact;
UNITY_LOCATION(0) uniform mediump sampler2D _BumpMap;
UNITY_LOCATION(1) uniform mediump sampler2D _CelStepTexture;
UNITY_LOCATION(2) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(3) uniform highp sampler2D _LightTexture0;
UNITY_LOCATION(4) uniform highp sampler2D _LightTextureB0;
UNITY_LOCATION(5) uniform mediump sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform mediump sampler2D _ShadowMapTexture;
in highp vec4 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp float vs_TEXCOORD6;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
vec4 u_xlat1;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
bool u_xlatb2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
mediump vec4 u_xlat16_4;
mediump float u_xlat16_5;
float u_xlat6;
mediump vec3 u_xlat16_6;
vec3 u_xlat8;
mediump float u_xlat16_8;
mediump float u_xlat16_10;
mediump float u_xlat16_16;
float u_xlat18;
mediump float u_xlat16_22;
void main()
{
    u_xlat16_0.xyz = texture(_BumpMap, vs_TEXCOORD0.zw).xyz;
    u_xlat16_1.xyz = u_xlat16_0.xyz * vec3(2.0, 2.0, 2.0) + vec3(-1.0, -1.0, -1.0);
    u_xlat0.x = dot(u_xlat16_1.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(u_xlat16_1.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(u_xlat16_1.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat18 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat0.xyz = vec3(u_xlat18) * u_xlat0.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_WorldToObject[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_WorldToObject[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_WorldToObject[2].z;
    u_xlat18 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat18 = inversesqrt(u_xlat18);
    u_xlat2.xyz = vec3(u_xlat18) * u_xlat2.xyz;
    u_xlat3.xyz = u_xlat0.yzx * u_xlat2.zxy;
    u_xlat1.xyz = u_xlat2.yzx * u_xlat0.zxy + (-u_xlat3.xyz);
    u_xlat0.x = dot(u_xlat2.xyz, u_xlat0.xyz);
    u_xlat1.w = u_xlat0.x + 1.0;
    u_xlat0.x = u_xlat1.w + u_xlat1.w;
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlat0 = u_xlat1 / u_xlat0.xxxx;
    u_xlat2.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat2.x = u_xlat0.w * u_xlat0.w + (-u_xlat2.x);
    u_xlat8.x = dot(u_xlat0.xyz, vs_TEXCOORD1.xyz);
    u_xlat1 = u_xlat0 + u_xlat0;
    u_xlat8.xyz = u_xlat8.xxx * u_xlat1.xyz;
    u_xlat2.xyz = vs_TEXCOORD1.xyz * u_xlat2.xxx + u_xlat8.xyz;
    u_xlat3.xyz = u_xlat0.zxy * vs_TEXCOORD1.yzx;
    u_xlat0.xyz = u_xlat0.yzx * vs_TEXCOORD1.zxy + (-u_xlat3.xyz);
    u_xlat0.xyz = u_xlat1.www * u_xlat0.xyz + u_xlat2.xyz;
    u_xlat16_4.x = dot(u_xlat0.xyz, vs_TEXCOORD3.xyz);
    u_xlat18 = (-_FlatRimSize) + 1.0;
    u_xlat18 = (-u_xlat16_4.x) * _FlatRimLightAlign + u_xlat18;
    u_xlat16_4.x = u_xlat16_4.x * 0.5 + 0.5;
    u_xlat2.x = _FlatRimEdgeSmoothness * 0.5 + u_xlat18;
    u_xlat18 = (-_FlatRimEdgeSmoothness) * 0.5 + u_xlat18;
    u_xlat2.x = (-u_xlat18) + u_xlat2.x;
    u_xlat2.x = float(1.0) / u_xlat2.x;
    u_xlat8.xyz = (-vs_TEXCOORD2.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat3.x = dot(u_xlat8.xyz, u_xlat8.xyz);
    u_xlat3.x = inversesqrt(u_xlat3.x);
    u_xlat3.xyz = u_xlat8.xyz * u_xlat3.xxx;
    u_xlat16_16 = dot(u_xlat3.xyz, u_xlat0.xyz);
    u_xlat16_16 = (-u_xlat16_16) + 1.0;
    u_xlat0.x = (-u_xlat18) + u_xlat16_16;
    u_xlat0.x = u_xlat2.x * u_xlat0.x;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat6 = u_xlat0.x * -2.0 + 3.0;
    u_xlat0.x = u_xlat0.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * u_xlat6;
    u_xlat16_16 = u_xlat16_4.x + (-_SelfShadingSizeExtra);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_16 = min(max(u_xlat16_16, 0.0), 1.0);
#else
    u_xlat16_16 = clamp(u_xlat16_16, 0.0, 1.0);
#endif
    u_xlat16_4.x = u_xlat16_4.x;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_4.x = min(max(u_xlat16_4.x, 0.0), 1.0);
#else
    u_xlat16_4.x = clamp(u_xlat16_4.x, 0.0, 1.0);
#endif
    u_xlat16_22 = float(1.0) / _ShadowEdgeSizeExtra;
    u_xlat16_22 = u_xlat16_22 * u_xlat16_16;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_22 = min(max(u_xlat16_22, 0.0), 1.0);
#else
    u_xlat16_22 = clamp(u_xlat16_22, 0.0, 1.0);
#endif
    u_xlat16_5 = u_xlat16_22 * -2.0 + 3.0;
    u_xlat16_22 = u_xlat16_22 * u_xlat16_22;
    u_xlat16_22 = u_xlat16_5 * u_xlat16_22 + (-u_xlat16_16);
    u_xlat16_16 = _FlatnessExtra * u_xlat16_22 + u_xlat16_16;
    u_xlat16_4.y = 0.5;
    u_xlat16_6.x = texture(_CelStepTexture, u_xlat16_4.xy).x;
    u_xlat16_4.xyw = _Color.xyz + (-_ColorDimSteps.xyz);
    u_xlat16_4.xyw = u_xlat16_6.xxx * u_xlat16_4.xyw + _ColorDimSteps.xyz;
    u_xlat16_4.xyw = u_xlat16_4.xyw + (-_ColorDimExtra.xyz);
    u_xlat16_4.xyz = vec3(u_xlat16_16) * u_xlat16_4.xyw + _ColorDimExtra.xyz;
    u_xlat16_6.xyz = (-u_xlat16_4.xyz) + _FlatRimColor.xyz;
    u_xlat0.xyz = u_xlat0.xxx * u_xlat16_6.xyz + u_xlat16_4.xyz;
    u_xlat16_3.xyz = texture(_MainTex, vs_TEXCOORD0.xy).xyz;
    u_xlat16_3.xyz = u_xlat16_3.xyz + vec3(-1.0, -1.0, -1.0);
    u_xlat16_3.xyz = vec3(_TextureImpact) * u_xlat16_3.xyz + vec3(1.0, 1.0, 1.0);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_3.xyz;
    u_xlat3.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat3.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat3.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat18 = dot(u_xlat8.xyz, u_xlat3.xyz);
    u_xlat2.xyz = vs_TEXCOORD2.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat2.x = sqrt(u_xlat2.x);
    u_xlat2.x = (-u_xlat18) + u_xlat2.x;
    u_xlat18 = unity_ShadowFadeCenterAndType.w * u_xlat2.x + u_xlat18;
    u_xlat18 = u_xlat18 * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat18 = min(max(u_xlat18, 0.0), 1.0);
#else
    u_xlat18 = clamp(u_xlat18, 0.0, 1.0);
#endif
    u_xlat1 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD2.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD2.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat2.xyz = u_xlat1.xyz / u_xlat1.www;
    u_xlat3.xyz = u_xlat2.xyz + _ShadowOffsets[0].xyz;
    vec3 txVec0 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    u_xlat3.xyz = u_xlat2.xyz + _ShadowOffsets[1].xyz;
    vec3 txVec1 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.y = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec1, 0.0);
    u_xlat3.xyz = u_xlat2.xyz + _ShadowOffsets[2].xyz;
    u_xlat2.xyz = u_xlat2.xyz + _ShadowOffsets[3].xyz;
    vec3 txVec2 = vec3(u_xlat2.xy,u_xlat2.z);
    u_xlat1.w = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec2, 0.0);
    vec3 txVec3 = vec3(u_xlat3.xy,u_xlat3.z);
    u_xlat1.z = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec3, 0.0);
    u_xlat2.x = dot(u_xlat1, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_8 = (-_LightShadowData.x) + 1.0;
    u_xlat2.x = u_xlat2.x * u_xlat16_8 + _LightShadowData.x;
    u_xlat16_4.x = (-u_xlat2.x) + 1.0;
    u_xlat16_4.x = u_xlat18 * u_xlat16_4.x + u_xlat2.x;
    u_xlat1 = vs_TEXCOORD2.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD2.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD2.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat2.xy = u_xlat1.xy / u_xlat1.ww;
    u_xlat2.xy = u_xlat2.xy + vec2(0.5, 0.5);
    u_xlat18 = texture(_LightTexture0, u_xlat2.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb2 = !!(0.0<u_xlat1.z);
#else
    u_xlatb2 = 0.0<u_xlat1.z;
#endif
    u_xlat8.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat8.x = texture(_LightTextureB0, u_xlat8.xx).x;
    u_xlat16_10 = (u_xlatb2) ? 1.0 : 0.0;
    u_xlat16_10 = u_xlat18 * u_xlat16_10;
    u_xlat16_10 = u_xlat8.x * u_xlat16_10;
    u_xlat16_4.x = u_xlat16_4.x * u_xlat16_10;
    u_xlat16_4.xyz = u_xlat16_4.xxx * _LightColor0.xyz;
    u_xlat16_4.xyz = u_xlat0.xyz * u_xlat16_4.xyz;
    u_xlat0.x = vs_TEXCOORD6;
#ifdef UNITY_ADRENO_ES3
    u_xlat0.x = min(max(u_xlat0.x, 0.0), 1.0);
#else
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
#endif
    u_xlat0.xyz = u_xlat16_4.xyz * u_xlat0.xxx;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
                                $Globals�        _WorldSpaceCameraPos                         _LightShadowData                       unity_ShadowFadeCenterAndType                           _LightColor0                  �     _ShadowOffsets                   �     _Color                    @     _FlatRimColor                     P     _FlatRimSize                  `     _FlatRimEdgeSmoothness                    d     _FlatRimLightAlign                    h     _SelfShadingSizeExtra                     l     _ShadowEdgeSizeExtra                  p     _FlatnessExtra                    t     _ColorDimSteps                    �     _ColorDimExtra                    �     _TextureImpact                    �     unity_WorldToShadow                       unity_WorldToObject                  0     unity_MatrixV                    p     unity_WorldToLight                             $Globals@        _WorldSpaceLightPos0                         unity_FogParams                   �      _MainTex_ST                         _BumpMap_ST                   0     unity_ObjectToWorld                        unity_WorldToObject                  P      unity_MatrixVP                   �      unity_WorldToLight                   �             _BumpMap                  _CelStepTexture                 _MainTex                _LightTexture0                  _LightTextureB0                 _ShadowMapTexture                