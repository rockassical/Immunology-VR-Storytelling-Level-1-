﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowCellInfo : MonoBehaviour
{

    public GameObject macrophageInfo;

    Vector3 hitPoint;
    Vector3 intersectPoint;
    
    
    // Start is called before the first frame update
    void Start()
    {
        macrophageInfo.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.forward, out hit, 500f))
        {
            hitPoint = hit.point;

            int layerMask = 1 << 12;
            RaycastHit hitInfo;
            if (Physics.Linecast(hitPoint, transform.position, out hitInfo, layerMask))
            {
                if(hitInfo.collider.tag == "Shell")
                {
                    intersectPoint = hitInfo.point;
                    
                }
            }

            if(hit.collider.tag == "Mac")
            {
                macrophageInfo.SetActive(true);
                macrophageInfo.transform.position = new Vector3(intersectPoint.x - 0.5f, intersectPoint.y, intersectPoint.z);
            }


        }




    }
}
