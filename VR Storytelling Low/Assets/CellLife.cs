﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellLife : MonoBehaviour
{

    public Slider healthBar;
    public Image fill;

    static public float currentLife = 500f;
    static public float maxLife = 900f;

    //public float extraLife;
    [SerializeField] float percent;
    [SerializeField] float lifeDecreaseSpeed;

    public bool increaseLife;
    //public bool decreaseLifeToxins;
    public bool decreaseLifeNets;

    bool checkCurrentLife;

    [SerializeField] float amountToDecrease;

    [SerializeField] float atMomentLife;

    [SerializeField] EmissionHighlightFlickering highlightingEffectScript;

    //public bool isNetsReset;

    [SerializeField] float setSpeed;

    // Start is called before the first frame update
    void Start()
    {
        //currentLife = currentLife + extraLife;
        healthBar.value = CalculateLife();

        lifeDecreaseSpeed = 0.5f;

        highlightingEffectScript = GetComponentInChildren<EmissionHighlightFlickering>();
    }

    // Update is called once per frame
    void Update()
    {

        healthBar.value = CalculateLife();

        currentLife -= Time.deltaTime * lifeDecreaseSpeed;

        percent = healthBar.value;

        LifeBarColor();

        if (percent < 0.2f)
        {
            lifeDecreaseSpeed = 0f;
        }

        if (increaseLife)
        {
            IncreaseLifeByIncrement();
        }

        if (decreaseLifeNets)
        {
            RapidDecreaseLifeAfterNETS();
        }
    }

    float CalculateLife()
    {
        return currentLife / maxLife;
    }

    void LifeBarColor()
    {
        if (CalculateLife() >= 0.4f)
        {
            fill.color = Color.green;

        }

        if (CalculateLife() < 0.4f && CalculateLife() >= 0.2f)
        {
            fill.color = Color.yellow;
            highlightingEffectScript.text.color = Color.yellow;
        }

        if (CalculateLife() < 0.2f && CalculateLife() >= 0f)
        {
            fill.color = Color.red;
            highlightingEffectScript.text.color = Color.red;
        }
    }

    public void IncreaseCellLife()
    {
        increaseLife = true;
    }

    void IncreaseLifeByIncrement()
    {

        if (currentLife < maxLife*0.9f)
        {
            currentLife += Time.deltaTime * 30f;
        }

        if (percent > 0.90f)
        {
            increaseLife = false;
            lifeDecreaseSpeed = 0.5f;

            highlightingEffectScript.FrameHighlightingOff();

        }

    }


    public void DecreaseCellLifeAfterNets(float speed)
    {
        decreaseLifeNets = true;
        setSpeed = speed;
        
    }

    /*
        void DecreaseLifeByIncrement()
        {
            if (!checkCurrentLife)
            {
                checkCurrentLife = true;

                amountToDecrease = percent * 900f * 0.4f;

                lifeDecreaseSpeed = 30f;

                atMomentLife = percent * 900f;

            }



            if ((atMomentLife - percent*900f) >=  amountToDecrease)
            {
                decreaseLifeToxins = false;
                lifeDecreaseSpeed = 0.5f;

                highlightingEffectScript.FrameHighlightingOff();

            }
        }

    */

    void RapidDecreaseLifeAfterNETS()
    {

        if (!checkCurrentLife)
        {
            checkCurrentLife = true;
            atMomentLife = currentLife;
        }


        lifeDecreaseSpeed = setSpeed;

        if (percent <= 0.1f)
        {
            lifeDecreaseSpeed = 0.5f;
            decreaseLifeNets = false;

            highlightingEffectScript.FrameHighlightingOff();
        }
    }


    public void RestoreCellLifeAfterNetsSim()
    {
        currentLife = atMomentLife;
        lifeDecreaseSpeed = 0.5f;
        highlightingEffectScript.text.color = Color.white;
    }
}
