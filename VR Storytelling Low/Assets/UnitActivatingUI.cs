﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitActivatingUI : MonoBehaviour
{

    public Slider progressBar;
    public Image fill_progress;

    public Slider lifeBar;
    public Image fill_life;

    
    public float currentProgress;
    public float maxProgress;

    public float currentLife;
    public float maxLife;

    

    [SerializeField] float percent_progress;
    [SerializeField] float percent_life;
 


    public bool increaseProgress;
    public bool increaseLife;


    // Start is called before the first frame update
    void Start()
    {
        progressBar.value = CalculateProgress();
        lifeBar.value = CalculateLife();
    }

    // Update is called once per frame
    void Update()
    {
        progressBar.value = CalculateProgress();
        percent_progress = progressBar.value;

        lifeBar.value = CalculateLife();
        percent_life = lifeBar.value;

        if (increaseProgress)
        {
            IncreaseProgressBar();
        }


        if (increaseLife)
        {
            IncreseCellLife();
        }



    }

    float CalculateProgress()
    {
        return currentProgress / maxProgress;
    }

    float CalculateLife()
    {
        return currentLife / maxLife;
    }


    public void UnitActivating()
    {
        increaseProgress = true;
        increaseLife = true;

    }

    void IncreaseProgressBar()
    {

        if (currentProgress < maxProgress)
        {
            currentProgress += Time.deltaTime * 30f;
        }

        if (percent_progress > 1f)
        {
            increaseProgress = false;
                                    
        }

    }


    void IncreseCellLife()
    {
        if (currentLife < maxLife * 0.6f)
        {
            currentLife += Time.deltaTime * 20f;
        }

        if (percent_life > 0.6f)
        {
            increaseLife = false;

        }
    }


}
