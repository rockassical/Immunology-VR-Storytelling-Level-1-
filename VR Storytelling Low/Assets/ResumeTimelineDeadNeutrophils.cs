﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


public class ResumeTimelineDeadNeutrophils : MonoBehaviour
{

    public PlayableDirector pDirector;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if(pDirector.state == PlayState.Paused)
            {
                pDirector.Resume();
                this.gameObject.GetComponent<SphereCollider>().isTrigger = false;

                Debug.Log("Waypoint 12 trigger entered");
            }
        }
    }
}
