﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EmissionHighlightFlickering : MonoBehaviour
{
    public TextMeshPro text;
    Renderer rend;
    //public Material matGlow;
    //Material matDefault;
    public bool isEmissionOn;
    public float interval;
    float lastTime;

    public bool isHighlighting;

    public bool isHighlightingOnWarning;


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        //matDefault = rend.material;
    }

    // Update is called once per frame
    void Update()
    {
        if (isHighlighting)
        {
            if(Time.time - lastTime > interval)
            {
                lastTime = Time.time;
                StartCoroutine(FrameHighlight(interval));
            }
        }


        if (isHighlightingOnWarning)
        {
            if (Time.time - lastTime > interval)
            {
                lastTime = Time.time;
                StartCoroutine(FrameHighlight_Warning(interval));
            }

        }

        if (!isHighlighting)
        {
            //rend.material = matDefault;
            rend.material.DisableKeyword("_EMISSION");
            //text.color = Color.white;
        }


        if (!isHighlightingOnWarning)
        {
            rend.material.DisableKeyword("_EMISSION");
            //text.color = Color.white;
        }

    }


    IEnumerator FrameHighlight(float time)
    {
        yield return new WaitForSeconds(time);
        isEmissionOn = !isEmissionOn;

        if (isEmissionOn)
        {
            //rend.material.SetColor("_EmissionColor", Color.red);
            rend.material.EnableKeyword("_EMISSION");
            //rend.material = matGlow;
            //text.color = Color.green;
        }
        else
        {
            rend.material.DisableKeyword("_EMISSION");
            //rend.material = matDefault;
            text.color = Color.white;
        }
    }


    IEnumerator FrameHighlight_Warning(float time)
    {
        rend.material.SetColor("_EmissionColor", Color.red);
       
        yield return new WaitForSeconds(time);
        isEmissionOn = !isEmissionOn;

        if (isEmissionOn)
        {
            rend.material.EnableKeyword("_EMISSION");
            //rend.material = matGlow;
            
        }
        else
        {
            rend.material.DisableKeyword("_EMISSION");
            //rend.material = matDefault;
            //text.color = Color.white;
        }
    }


    public void FrameHighlightingOn()
    {
        isHighlighting = true;
    }


    public void FrameHighlightingOn_Warning()
    {
        isHighlightingOnWarning = true;
    }


    public void FrameHighlightingOff()
    {
        
        //rend.material = matDefault;
        isHighlighting = false;
        isHighlightingOnWarning = false;
        isEmissionOn = false;
        //rend.material.DisableKeyword("_EMISSION");

    }
}
