﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractivityOn : MonoBehaviour
{
    
    public GameObject leftController;
    public bool isScriptsOn;
    

    private ThumbstickMove thumbstickMove;
    private CollisionAvoidance colliAvoid;
    

    private void Awake()
    {
        thumbstickMove = leftController.GetComponent<ThumbstickMove>();
        colliAvoid = leftController.GetComponent<CollisionAvoidance>();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && isScriptsOn==false)
        {
            StartCoroutine(EnableMoveScripts(3));
            isScriptsOn = true;

            Debug.Log("coroutine is called");
        }
    }



    IEnumerator EnableMoveScripts (float time) 
    {
        yield return new WaitForSeconds(time);

        thumbstickMove.enabled = true;

        colliAvoid.enabled = true;
    }


}
