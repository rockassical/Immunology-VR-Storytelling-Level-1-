﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class TimelineResumeTrigger : MonoBehaviour
{
    public PlayableDirector pDirector;
    float timer = 1f;
    public bool isTimelineResumed;
    public bool startCheckingTimeline;

    BoxCollider col;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<BoxCollider>();
    }

    private void Update()
    {
        if (startCheckingTimeline)
        {
            CheckTimelineStatus();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            
            if(pDirector.state == PlayState.Paused && !isTimelineResumed)
            {
                isTimelineResumed = true;
                pDirector.Resume();
                col.isTrigger = false;
            }

            if (pDirector.state == PlayState.Playing && !isTimelineResumed)
            {
                startCheckingTimeline = true;
            }


        }
    }

    void CheckTimelineStatus()
    {
        timer -= Time.deltaTime;

        if (timer < 0f)
        {
            if (pDirector.state == PlayState.Playing)
            {
                timer = 1f;
            }
            else
            {
                timer = 0f;
                startCheckingTimeline = false;
                if (!isTimelineResumed)
                {
                    pDirector.Resume();
                    isTimelineResumed = true;
                }

            }
        }

    }

}
