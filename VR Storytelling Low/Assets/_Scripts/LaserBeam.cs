﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;
using TMPro;



public class LaserBeam : MonoBehaviour
{
    public GameObject laserOrigin;
    public LineRenderer laser;

    public bool triggerL;

    public Vector3 hitPoint;

    public GameObject bacteriumInfo;
    public GameObject otherInfo;

    [SerializeField] bool isPathogenIdentified;
    [SerializeField] AudioSource audioPrompt;
    public AudioClip promptAudio;

    public PlayableDirector pdirector;
    
    bool isTimelineResumed;

    public ScanBacteriumMutant scanScript;

    public GameObject progressUI;
    public Image progressCircle;
    public float totalTime;
    [SerializeField] float timeLeft;

    public TextMeshPro text;

    private void Start()
    {
        progressUI.SetActive(false);
        laser.enabled = false;
        audioPrompt = GetComponent<AudioSource>();
        timeLeft = totalTime;
    }

    // Update is called once per frame
    void Update()
    {
        
        triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);
        //triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);

        if (triggerL)
        {
            laser.enabled = true;

            laser.widthMultiplier = 0.2f;

            laser.SetPosition(0, laserOrigin.transform.position + laserOrigin.transform.forward * 0.1f);
            laser.SetPosition(1, laserOrigin.transform.position + laserOrigin.transform.forward * 15f);

            RaycastHit hit;

            if (Physics.Raycast(laserOrigin.transform.position, transform.forward, out hit, 1000))
            {
                hitPoint = hit.point;

                Debug.DrawRay(laserOrigin.transform.position, hitPoint, Color.green);

                

                if (hit.collider.tag == "S A Mutant" )
                {

                    if (!isPathogenIdentified)
                    {
                        text.color = Color.white;
                        text.text = "hold trigger to identify bacterium";
                        progressUI.SetActive(true);
                        timeLeft -= Time.deltaTime;
                        progressCircle.fillAmount += 1f / totalTime * Time.deltaTime;

                        if (timeLeft < 0f)
                        {
                            audioPrompt.PlayOneShot(promptAudio);
                            progressCircle.fillAmount = 0f;
                            progressUI.SetActive(false);
                            //text.fontSize = 19.78f;
                            text.color = Color.green;
                            text.text = "bacterium identification completed";

                            isPathogenIdentified = true;
                        }

                    }
                    else
                    {
                        text.text = "bacterium identification completed";
                    }

                }
                else
                {
                    progressCircle.fillAmount = 0f;
                    progressUI.SetActive(false);
                    timeLeft = totalTime;
                }

            }
            else
            {
                laser.enabled = false;
                progressCircle.fillAmount = 0f;
                progressUI.SetActive(false);
                timeLeft = totalTime;
            }

        }
        else
        {
            laser.enabled = false;
            progressCircle.fillAmount = 0f;
            progressUI.SetActive(false);
            timeLeft = totalTime;
        }


        if (isPathogenIdentified && !isTimelineResumed)
        {
            if (pdirector.state == PlayState.Paused)
            {
                otherInfo.SetActive(false);
                bacteriumInfo.SetActive(true);
                pdirector.Resume();
                isTimelineResumed = true;
            }

        }

    }

    
    
}
