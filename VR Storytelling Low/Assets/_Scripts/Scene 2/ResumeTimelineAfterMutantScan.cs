﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ResumeTimelineAfterMutantScan : MonoBehaviour
{
    public GameObject timeline;
    
    [SerializeField] PlayableDirector pDirector;

    bool isTimelinePlaying;
    // Start is called before the first frame update
    void Start()
    {
        pDirector = timeline.GetComponent<PlayableDirector>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.activeSelf && !isTimelinePlaying)
        {
            StartCoroutine(ResumeTimeline(2f));
            isTimelinePlaying = true;
        }
    }


    IEnumerator ResumeTimeline(float t)
    {
        yield return new WaitForSeconds(t);

        if(pDirector.state == PlayState.Paused)
        {
            pDirector.Resume();
        }
    }
}
