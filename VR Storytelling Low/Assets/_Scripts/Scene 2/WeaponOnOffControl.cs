﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponOnOffControl : MonoBehaviour
{

    [SerializeField] Degranulation degranulation;
    [SerializeField] Phagocytosis phagocytosis;
    [SerializeField] NETS nets;
    [SerializeField] WeaponToggleSwitch weaponSwitch;



    void Start()
    {
        degranulation = GetComponentInChildren<Degranulation>();
        phagocytosis = GetComponentInChildren<Phagocytosis>();
        nets = GetComponentInChildren<NETS>();
        weaponSwitch = GetComponentInChildren<WeaponToggleSwitch>();

    }

    
    public void TurnOnWeapon()
    {
        weaponSwitch.enabled = true;
    }

    public void TurnOffWeapon()
    {
        weaponSwitch.enabled = false;
        degranulation.enabled = false;
        phagocytosis.enabled = false;
        nets.enabled = false;
    }
}
