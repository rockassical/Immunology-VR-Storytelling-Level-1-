﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using TMPro;
using UnityEngine.UI;

public class ScanBacteriumMutant : MonoBehaviour
{

    SpriteRenderer rend;

    public bool isColorChange;
    public float interval;
    public bool startScan;
    public float timer;
    public GameObject scanResult;
    public GameObject previousInfo;
    public AudioSource popupSound;
    public AudioClip UIsound;
    //public AudioClip scanSound;

    Color originColor;
    float lastTime;

    public PlayableDirector pdirector;
    public bool isTimelineResumed;

    public GameObject progressUI;
    public Image progressCircle;
    public float totalTime;
    [SerializeField] float timeLeft;

    public TextMeshPro text;

    public bool startProcessingBacInfo;
    public bool isBacIdentified;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        originColor = rend.color;

        progressUI.SetActive(false);
        timeLeft = totalTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastTime > interval && startScan)
        {
            lastTime = Time.time;
            StartCoroutine(FlickeringColor(interval));
        }

        if (startScan)
        {
            timer -= Time.deltaTime;
        }


        if (timer <= 0f)
        {
            startScan = false;
            
            //previousInfo.SetActive(false);
            //scanResult.SetActive(true);
            //popupSound.PlayOneShot(UIsound, 2f);

            startProcessingBacInfo = true;
            timer = 0.1f;

        }


        if (startProcessingBacInfo)
        {

            if (!isBacIdentified)
            {
                StartCoroutine(ProcessBacteriumInfomation(2f));
            }

        }


        if (isBacIdentified && !isTimelineResumed)
        {
            if (pdirector.state == PlayState.Paused)
            {
                progressUI.SetActive(false);
                previousInfo.SetActive(false);
                scanResult.SetActive(true);
                popupSound.PlayOneShot(UIsound);
                pdirector.Resume();
                isTimelineResumed = true;
                
                this.gameObject.SetActive(false);
            }
        }

    }

    IEnumerator FlickeringColor(float time)
    {
        yield return new WaitForSeconds(time);
        isColorChange = !isColorChange;

        if (isColorChange)
        {
            rend.material.color = Color.red;
        }
        else
        {
            rend.material.color = originColor;
        }

    }

    public void StartBacScanning()
    {
        startScan = true;
    }

    IEnumerator ProcessBacteriumInfomation(float t)
    {
        yield return new WaitForSeconds(t);

        progressUI.SetActive(true);
        timeLeft -= Time.deltaTime;
        progressCircle.fillAmount += 1f / totalTime * Time.deltaTime;

        if (timeLeft < 0f && startProcessingBacInfo)
        {
            
            //progressUI.SetActive(false);

            
            isBacIdentified = true;
            startProcessingBacInfo = false;
        }

    }
}
