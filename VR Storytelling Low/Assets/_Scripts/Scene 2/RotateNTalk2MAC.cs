﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateNTalk2MAC : MonoBehaviour
{

    [SerializeField] Transform mac;
    [SerializeField] Transform philOrigin;
    [SerializeField] float turningSpeed = 0.5f;
    public bool turnToMAC;
    public bool backToRoute;


    
    // Update is called once per frame
    void Update()
    {
        if (turnToMAC)
        {
            TurnToFacePHIL();

        }

        if (backToRoute)
        {
            TurnBackToRoute();
        }
    }


    public void PHILTurnsToMAC()
    {
        turnToMAC = true;
        backToRoute = false;
    }

    public void PHILTurnsBackToRoute()
    {
        backToRoute = true;
        turnToMAC = false;
    }


    void TurnToFacePHIL()
    {
        Vector3 directionToFace = mac.position - transform.position;
        Debug.DrawRay(transform.position, directionToFace, Color.green);
        Quaternion targetRotation = Quaternion.LookRotation(directionToFace);

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime*turningSpeed);


    }

    void TurnBackToRoute()
    {
        Vector3 directionToTurnback = philOrigin.position - transform.position;
        Debug.DrawRay(transform.position, directionToTurnback, Color.red);
        Quaternion rotationBack = Quaternion.LookRotation(directionToTurnback);

        //transform.rotation = Quaternion.RotateTowards(transform.rotation, rotationBack, Time.deltaTime*20f);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotationBack, Time.deltaTime* turningSpeed);


    }
}
