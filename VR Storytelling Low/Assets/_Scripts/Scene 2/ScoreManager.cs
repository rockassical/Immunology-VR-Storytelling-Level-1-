﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{


    public static int score;
    public static int killCount;
    public TextMeshPro killCountText;
   
    int preKillCount;
    [SerializeField] AudioSource uiSound;
    public AudioClip enemyDeathSound;
    public GameObject enemyKilledTextPrefab;
    public Transform textPos;


    // Use this for initialization
    void Start()
    {
        killCount = 0;
        preKillCount = killCount;
        uiSound = GetComponentInParent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (killCountText != null)
        {
            killCountText.text = "Enemy Killed:  " + killCount;
        }

        if(killCount - preKillCount == 1)
        {
            GameObject TerminationText = Instantiate(enemyKilledTextPrefab, textPos.position, textPos.rotation);
            TerminationText.transform.parent = textPos.transform;
            Destroy(TerminationText, 2f);

            uiSound.PlayOneShot(enemyDeathSound);
            //Debug.Log("ONE ENEMY KILLED");
            preKillCount = killCount;
        }

    }
}
