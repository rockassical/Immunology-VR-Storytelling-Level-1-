﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CountDownTimerPlusSound : MonoBehaviour
{
    public int Minutes = 1;
    public int Seconds = 0;

    //public Text m_text;
    public float m_leftTime;

    public bool isCountDownStart;
    public TextMeshPro timerText;

    [SerializeField] bool isTimerReset;

    public AudioClip beepingSound;
    AudioSource countdownAudio;

    bool isBeepinglayed;
 





    private void Awake()
    {
        //m_text = GetComponent<Text>();
        m_leftTime = GetInitialTime();

        countdownAudio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (m_leftTime > 0f && isCountDownStart)
        {
            if (isTimerReset)
            {
                m_leftTime = GetInitialTime();
                isTimerReset = false;
            }

            //  Update countdown clock
            m_leftTime -= Time.deltaTime;
            Minutes = GetLeftMinutes();
            Seconds = GetLeftSeconds();

            //  Show current clock
            if (m_leftTime > 0f)
            {
                timerText.text = Minutes + ":" + Seconds.ToString("00");
            }
            else
            {
                //  The countdown clock has finished
                timerText.text = "0:00";
            }
        }


        if(Seconds == 59 && !isBeepinglayed)
        {
            isBeepinglayed = true;

            InvokeRepeating("PlayBeepingSound", 0f, 1f);
        }

        
    }


    private float GetInitialTime()
    {
        return Minutes * 60f + Seconds;
    }

    private int GetLeftMinutes()
    {
        return Mathf.FloorToInt(m_leftTime / 60f);
    }

    private int GetLeftSeconds()
    {
        return Mathf.FloorToInt(m_leftTime % 60f);
    }


    public void StartCountdown()
    {
        isCountDownStart = true;

    }

    public void StopCountdown()
    {
        isCountDownStart = false;
        CancelInvoke();
    }

    public void ResetCountdown()
    {
        isTimerReset = true;
        Minutes = 1;
        Seconds = 0;
        isBeepinglayed = false;
    }

    void PlayBeepingSound()
    {
        countdownAudio.PlayOneShot(beepingSound);
    }
}
