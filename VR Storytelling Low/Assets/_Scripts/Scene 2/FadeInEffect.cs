﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FadeInEffect : MonoBehaviour
{

    [SerializeField] Renderer[] rendChildren;
    public bool startFadeIn;
    
    private void Awake()
    {
        rendChildren = GetComponentsInChildren<Renderer>();

        foreach (Renderer rend in rendChildren)
        {
            rend.material.DOFade(0.0F, 0.1F);
        }
    }

    private void Start()
    {
        //ShowFadeInEffect();
    }
    // Update is called once per frame
    void Update()
    {
        
    }


    public void FadeIn()
    {
        rendChildren = GetComponentsInChildren<Renderer>();

        foreach (Renderer rend in rendChildren)
        {
            rend.material.DOFade(1.0F, 5F);
        }
    }

    void ShowFadeInEffect()
    {
        rendChildren = GetComponentsInChildren<Renderer>();

        foreach (Renderer rend in rendChildren)
        {
            rend.material.DOFade(1.0F, 5F);
        }

    }

}
