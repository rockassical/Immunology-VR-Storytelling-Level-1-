﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class CompleteTaskWarning : MonoBehaviour
{

    public GameObject warningInfo;
    public GameObject[] weaponInfo;
    public AudioSource warningAudio;
    public AudioClip audioclip;
    [SerializeField]float timer;
    public float timerReset;
    public bool isTimerStarted;
    public bool isWarningShown;
    public float waitTime;
    splineMove splineScript;
    [SerializeField] int killCount;

    // Start is called before the first frame update
    void Start()
    {
        timer = timerReset;
        splineScript = GetComponentInParent<splineMove>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTimerStarted)
        {
            
            timer -= Time.deltaTime;

            if (OVRInput.GetDown(OVRInput.RawButton.LIndexTrigger) || OVRInput.Get(OVRInput.RawButton.LIndexTrigger) || splineScript.speed > 1f || (ScoreManager.killCount - killCount == 1))
            {
                isTimerStarted = false;
                timer = timerReset;
            }

        }

        if(timer < (timerReset - waitTime) && !isWarningShown)
        {
            foreach(GameObject g in weaponInfo)
            {
                if (g.activeSelf)
                {
                    g.SetActive(false);
                }
            }

            isWarningShown = true;
            warningInfo.SetActive(true);
            warningAudio.PlayOneShot(audioclip);

            StartCoroutine(RestorePreviousInfo(4f));
        }

        if(timer < 0f)
        {
            timer = timerReset;
        }
    }

    public void StartCompleteTaskTimer()
    {
        if (!isTimerStarted)
        {
            timer = timerReset;
            isTimerStarted = true;
            killCount = ScoreManager.killCount;
        }

        if (isWarningShown)
        {
            isWarningShown = false;
        }
        
    }


    IEnumerator RestorePreviousInfo(float t)
    {
        yield return new WaitForSeconds(t);

        warningInfo.SetActive(false);

        foreach(GameObject g in weaponInfo)
        {
            if (!g.activeSelf)
            {
                g.SetActive(true);
            }
        }
    }

}
