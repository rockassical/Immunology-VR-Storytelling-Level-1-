﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using TMPro;
using SWS;

public class ConditionTransitions : MonoBehaviour
{

    public GameObject player;
    public GameObject timelineS2;


    PlayableDirector pDirector;
    splineMove spline;
    public splineMove splineMAC;
    //public WeaponToggleSwitch weaponToggleScript;
    public TimerCountdown timerScript;
    //public Degranulation degranulationScript;
    public Phagocytosis phagocytosisScript;
    public GameObject bacteriumIngestedInfo;
  
    //weapon graphics
    public GameObject aimingReticleDegranulation;
    public GameObject aimingReticleNETS;
    public GameObject D_button;
    public GameObject P_button;
    public GameObject N_button;
    public Material buttonUnlit;
    public GameObject weaponInfos;
    public GameObject weaponPropertyGraphic;

    Vector3 turnDirection;
    //bool isMoving;
    public bool isDegranulationDone;
    public bool isKill2MoreUsingDegranulationDone;
    public bool isPhagocytosisDone;
    public bool isKillcount7;
    public bool isNetsSimulationDone;

    public float speedToPhagocytosis;
    //  public float speedAfterNetsSim;

    WeaponOnOffControl weaponOnOffScript;

   public bool isTimelineResumed;

    private void Awake()
    {
        pDirector = timelineS2.GetComponent<PlayableDirector>();
        spline = GetComponentInParent<splineMove>();

        bacteriumIngestedInfo.SetActive(false);
        weaponOnOffScript = GetComponent<WeaponOnOffControl>();
    }
    // Start is called before the first frame update
    void Start()
    {
        timerScript.isCountDownStart = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (ScoreManager.killCount == 1 && pDirector.state == PlayState.Paused && !isDegranulationDone)
        {
                      

            pDirector.Resume();

            //WeaponElementsOff();
            weaponInfos.SetActive(false);
            weaponPropertyGraphic.SetActive(false);

            isDegranulationDone = true;
        }


        if(ScoreManager.killCount == 3 && !isKill2MoreUsingDegranulationDone)
        {
            pDirector.Resume();
            MoveToRoute();

            StartCoroutine(ResumeRouteAfterDegranulation());
        }

        if(ScoreManager.killCount == 3 && phagocytosisScript.startIngestion && !isTimelineResumed && pDirector.state == PlayState.Paused && phagocytosisScript.isMoving)
        {
            pDirector.Resume();
            isTimelineResumed = true;

            Debug.Log("TIMELINE RESUMED AT " + pDirector.time);
        }

        if (ScoreManager.killCount == 4 && !isPhagocytosisDone)
        {
            phagocytosisScript.isIngestingNow = true;
            pDirector.Resume();
            
            Debug.Log("TIMELINE AFTER PHAGOCYTOSIS RESUMED AT " + pDirector.time);

            MoveToRoute();

            StartCoroutine(ResumeRouteAfterPhagocytosis());


            //weaponToggleScript.isUnlocked = false;
        }


        if ((ScoreManager.killCount == 7 && !isKillcount7))
        {
            phagocytosisScript.isIngestingNow = true;

            pDirector.Resume();
            MoveToRoute();

            StartCoroutine(ResumeRouteAfterKillcountIs7());


            //weaponToggleScript.isUnlocked = false;
        }

        if (isNetsSimulationDone)
        {
            StartCoroutine(ResumeRouteAfterNetsSimulation());
        }

    }



    void TurnBackToRoute()
    {
        turnDirection = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 0.5f);

    }

    void MoveToRoute()
    {
        turnDirection = player.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 15f * Time.deltaTime);

       // if (transform.rotation.Compare(rotation, 10 ^ -3))
       // {
       //      transform.rotation = player.transform.rotation;
       // }

    }



    IEnumerator ResumeRouteAfterDegranulation()
    {
        
        yield return new WaitForSeconds(3f);
        spline.ChangeSpeed(speedToPhagocytosis);
        spline.Resume();
        //splineMAC.Resume();
        
        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isKill2MoreUsingDegranulationDone = true;

    }

    IEnumerator ResumeRouteAfterPhagocytosis()
    {
        
        yield return new WaitForSeconds(3f);
        spline.Resume();
        splineMAC.Resume();

        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isPhagocytosisDone = true;

        bacteriumIngestedInfo.SetActive(true);
        weaponOnOffScript.TurnOffWeapon();

        yield return new WaitForSeconds(2f);
        phagocytosisScript.isIngestingNow = false;
    }


    IEnumerator ResumeRouteAfterKillcountIs7()
    {
        
        yield return new WaitForSeconds(3f);
        spline.Resume();
        //splineMAC.Resume();

        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isKillcount7 = true;

        yield return new WaitForSeconds(2f);
        phagocytosisScript.isIngestingNow = false;

    }

    IEnumerator ResumeRouteAfterNetsSimulation()
    {
        yield return new WaitForSeconds(2f);
        TurnBackToRoute();

        yield return new WaitForSeconds(2f);
        WeaponElementsOff();
        weaponInfos.SetActive(false);
        weaponPropertyGraphic.SetActive(false);
        isNetsSimulationDone = false;
        
    }


    public void WeaponElementsOff()
    {
        aimingReticleDegranulation.SetActive(false);
        aimingReticleNETS.SetActive(false);

        D_button.GetComponent<Renderer>().material = buttonUnlit;
        P_button.GetComponent<Renderer>().material = buttonUnlit;
        N_button.GetComponent<Renderer>().material = buttonUnlit;
    }



}


