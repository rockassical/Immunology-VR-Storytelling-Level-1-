﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class CalculateTimeOnSplineMove : MonoBehaviour
{

    [SerializeField] float timePassed;
    [SerializeField] float initialTime;
    splineMove spline;
    public int startLocation;
        
    
    // Start is called before the first frame update
    void Start()
    {

        initialTime = Time.time;
        spline = GetComponent<splineMove>();
        spline.startPoint = startLocation;

        
        spline.StartMove();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        // Scene 1 triggers
        if (other.tag == "Stop Trigger")
        {
            spline.Pause();
            timePassed = initialTime + Time.time;
            Debug.Log("stop trigger hit! " + timePassed);
        }

        
        if (other.tag == "Selectin")
        {
            Debug.Log("Selectin hit time " + Time.time);
        }


        // Scene 2 triggers
        if (other.tag == "Phagocytosis Trigger")
        {
            spline.Pause();
            timePassed = initialTime + Time.time;

            Debug.Log("phagocytosis trigger hit! " + timePassed);
        }

        if(other.tag == "NETS Unlock Trigger")
        {
            spline.Pause();
            timePassed = initialTime + Time.time;
            Debug.Log("NETS unlock trigger hit! " + timePassed);
        }

        if (other.tag == "Waypoint 13")
        {
            spline.Pause();
            timePassed = initialTime + Time.time;
        }


        if (other.tag == "Dead Neutrophils Trigger")
        {
            spline.Pause();
            timePassed = initialTime + Time.time;
        }


        if (other.tag == "Seeing Mutant Trigger")
        {
            initialTime = Time.time;
        }

        if(other.tag == "PHIL Path End")
        {
            timePassed = Time.time - initialTime;
        }

    }
}
