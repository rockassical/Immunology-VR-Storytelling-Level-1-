﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutantScanTrigger : MonoBehaviour
{

    public GameObject scanReticle;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            scanReticle.SetActive(true);
            scanReticle.GetComponent<ScanBacteriumMutant>().startScan = true;

            this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }

}
