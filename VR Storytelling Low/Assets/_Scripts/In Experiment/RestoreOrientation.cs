﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestoreOrientation : MonoBehaviour
{

   
    public Transform resetRef;
    [SerializeField]Rigidbody rb;
    [SerializeField] bool startRestoringOrientation;
    Vector3 turnDirection;
    [SerializeField]float angle;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
     
        //resetRef = Instantiate(groupCenter, transform.position + transform.forward * 0.5f, transform.rotation);
        //resetRef.SetParent(transform);
    }

    // Update is called once per frame
    void Update()    
    {
        if (startRestoringOrientation)
        {
            
            StartCoroutine(RestoreObjectRotation(5f));
            
        }

        angle = Quaternion.Angle(transform.rotation, resetRef.rotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        startRestoringOrientation = true;
    }




    IEnumerator RestoreObjectRotation(float t)
    {
        yield return new WaitForSeconds(t);

        rb.useGravity = false;
        rb.isKinematic = true;

        angle = Quaternion.Angle(transform.rotation, resetRef.rotation);


        turnDirection = resetRef.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(turnDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 10f);



        if (angle < 1f)
        {
            startRestoringOrientation = false;
            rb.isKinematic = false;
            rb.useGravity = true;
        }

    }
}
