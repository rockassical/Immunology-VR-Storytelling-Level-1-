﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using SWS;

public class ObjectCollision : MonoBehaviour
{

    public AudioSource audio;
    public AudioClip clip;
    [SerializeField] splineMove spline;
    [SerializeField] float currentSpeed;
    public PlayableDirector pdirector;

    // Start is called before the first frame update
    void Start()
    {
        spline = GetComponentInParent<splineMove>();
        currentSpeed = this.gameObject.GetComponentInParent<splineMove>().speed;

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Selectin_reduce speed")
        {
            
            this.gameObject.GetComponent<Animator>().SetTrigger("PHIL Selectin Hit on Right");
            audio.PlayOneShot(clip);

            //spline.ChangeSpeed(currentSpeed - 4.5f);
           
            pdirector.Resume();

            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }



        if (other.tag == "Selectin")
        {
            this.gameObject.GetComponent<Animator>().SetTrigger("PHIL Selectin Hit on Right");
            audio.PlayOneShot(clip);

            currentSpeed = spline.speed;
            spline.ChangeSpeed(currentSpeed - 2f);

            other.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }

        if(other.tag == "Stop Trigger")
        {
            audio.PlayOneShot(clip);
        }

    }
}
