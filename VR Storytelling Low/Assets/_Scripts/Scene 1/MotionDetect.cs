﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class MotionDetect : MonoBehaviour
{

    public GameObject timeline;
    public GameObject player;
    public Vector3 prePos;
    public Vector3 currPos;
    public float timer = 1f;
    public float posDiffThreshold = 40f;

    public bool isPosTracked;
    public bool isMoving;

    private PlayableDirector playableDirector;



    private void Awake()
    {
        playableDirector = timeline.GetComponent<PlayableDirector>();

    }



    // Update is called once per frame
    void Update()
    {

        if (isPosTracked && !isMoving)
        {
            timer -= Time.deltaTime;

            if (timer <= 0f)
            {

                currPos = player.transform.position;
                timer = 1f;

                //Debug.Log("currPos is" + currPos);
                //Debug.Log("Position difference is " + (currPos - prePos).sqrMagnitude);

                if ((currPos - prePos).sqrMagnitude > posDiffThreshold && playableDirector.state == PlayState.Paused)
                {
                    isMoving = true;

                    playableDirector.Resume(); //starts from PHIL_S1_11, Here we go, I can finally move by myself!


                }
            }
        }


    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Stop Trigger")
        {
            currPos = player.transform.position;
            prePos = currPos;
            other.isTrigger = false;
            isPosTracked = true;

            //Debug.Log("prePos is " + prePos);
        }
    }


}
