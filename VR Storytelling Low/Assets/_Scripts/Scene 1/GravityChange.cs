﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityChange : MonoBehaviour
{

    //Transform trans;
    
    // Start is called before the first frame update
    void Start()
    {

        Physics.gravity = Vector3.zero;
        
        foreach(Transform t in transform)
        {

            foreach (Transform t_child in t.transform)
            {
                
                if(t_child.GetComponent<MeshCollider>() != null)
                {
                    t_child.GetComponent<MeshCollider>().convex = true;
                }
                
            }

            t.gameObject.AddComponent<Rigidbody>();
            Rigidbody rb = t.gameObject.GetComponent<Rigidbody>();
            rb.drag = 100f;
            rb.angularDrag = 50f;

            
        }
    }

   
}
