﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class ResumeTimelineOnPhilsLanding : MonoBehaviour
{

    public GameObject timeline;
    public float timeDelayed = 2f;
    PlayableDirector playableDirector;
    [SerializeField] Animator animPHIL;

    // Start is called before the first frame update
    void Start()
    {
        playableDirector = timeline.GetComponent<PlayableDirector>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && playableDirector.state == PlayState.Paused)
        {
            animPHIL = other.gameObject.GetComponent<Animator>();
            animPHIL.SetTrigger("PHIL Lands on Surface");
            
            StartCoroutine(ResumeTimeline(timeDelayed));

            this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }

    IEnumerator ResumeTimeline(float t) //starts from PHIL_S1_10, That's a rough landing!
    {
        yield return new WaitForSeconds(t);
        playableDirector.Resume();
    }
}
