﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableControllerScripts : MonoBehaviour
{

    ThumbstickMove thumbStickMove;
    CollisionAvoidance colliAvoid;

    private void Awake()
    {
        thumbStickMove = this.GetComponent<ThumbstickMove>();
        colliAvoid = this.GetComponent<CollisionAvoidance>();

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void EnableMoveScripts()
    {

        thumbStickMove.enabled = true;
        colliAvoid.enabled = true;
    }
}
