﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using SWS;
using TMPro;

public class ExperienceSetupAndStart : MonoBehaviour
{
    //public GameObject playerVisuals;
    public GameObject playerOnTimeline;
    //public GameObject timelineMainS1;
    public GameObject timeline2nd;
    //PlayableDirector playableDirectorMainS1;
    PlayableDirector playableDirector2nd;
    public splineMove splinePHIL;
    public splineMove splineMONO6;
    Animator animPHIL;

    //controller laser beam
    public GameObject laserOrigin;
    LineRenderer laser;
    public bool triggerL;

    public Transform touchL;


    public bool inputA;
    public bool inputB;

    public Vector3 hitPoint;

    public bool startGame;

    public GameObject openingText;

    private void Awake()
    {
        //playerVisuals.SetActive(false);
        
        laser = GetComponent<LineRenderer>();
        laser.enabled = false;

        //playableDirectorMainS1 = timelineMainS1.GetComponent<PlayableDirector>();
        playableDirector2nd = timeline2nd.GetComponent<PlayableDirector>();
        animPHIL = playerOnTimeline.GetComponent<Animator>();

        openingText.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {

        touchL.localPosition = OVRInput.GetLocalControllerPosition(OVRInput.Controller.LTouch);
        touchL.localRotation = OVRInput.GetLocalControllerRotation(OVRInput.Controller.LTouch);


        inputA = OVRInput.Get(OVRInput.RawButton.A);
        inputB = OVRInput.Get(OVRInput.RawButton.B);


        triggerL = OVRInput.Get(OVRInput.RawButton.LIndexTrigger);

/*
        if (triggerL)
        {
            RaycastHit hit;

            if (Physics.Raycast(laserOrigin.transform.position, transform.forward, out hit, 1000))
            {
                hitPoint = hit.point;

                //Debug.DrawRay(laserOrigin.transform.position, hitPoint, Color.green);

                laser.enabled = true;

                laser.widthMultiplier = 0.2f;

                laser.SetPosition(0, laserOrigin.transform.position);
                laser.SetPosition(1, hitPoint);

                if (hit.collider.tag == "Start")
                {
                    Debug.Log("START IS HIT");
                    startGame = true;
                    hit.collider.enabled = false;
                    hit.collider.gameObject.GetComponent<Animator>().SetTrigger("Fade text");
                    Destroy(hit.collider.gameObject, 2f);
                    
                }

            }
            else
            {
                laser.enabled = false;
            }

        }
        else
        {
            laser.enabled = false;
        }
*/

        if (startGame == true)
        {
            StartCoroutine(StartExperience());
        }
        

    }




    IEnumerator StartExperience()
    {
        startGame = false;
        //playerVisuals.SetActive(true);
        animPHIL.SetTrigger("PHIL Drop Start");
       
        yield return new WaitForSeconds(2f);

        //playableDirectorMainS1.Play();
        playableDirector2nd.Play();
        splineMONO6.StartMove();
        splinePHIL.StartMove();

        openingText.SetActive(false);

    }

    public void StartScene1()
    {
        startGame = true;
    }

}
