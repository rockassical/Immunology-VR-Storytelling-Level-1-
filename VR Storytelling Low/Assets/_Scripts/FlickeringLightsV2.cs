﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickeringLightsV2 : MonoBehaviour {

    public GameObject[] statusLights;
    GameObject lightSelected;
    
    //Renderer rend;
    public bool isLightOn;
    public float minTime;
    public float maxTime;
    public float randomTime;


    private void Awake()
    {
        //rend = GetComponent<Renderer>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int indexSelected = Random.Range(0, statusLights.Length);
        lightSelected = statusLights[indexSelected];

        randomTime = Random.Range(minTime, maxTime);

        StartCoroutine(FlickeringLight(randomTime));

    }


    IEnumerator FlickeringLight(float time)
    {
        yield return new WaitForSeconds(time);
        isLightOn = !isLightOn;

        if (isLightOn)
        {
            lightSelected.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        }
        else
        {
            lightSelected.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
        }

    }

}
