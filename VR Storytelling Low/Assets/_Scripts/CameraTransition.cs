﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    public int cameraView = 0;
    public float speed = 3;
    public GameObject PHIL;
    public GameObject player;

    /*public bool firstPersonActive = true;*/
    public GameObject firstPersonView;
    public GameObject thirdPersonView;
    

    private GameObject[] povs;

    // Start is called before the first frame update
    void Start()
    {
        PHIL.SetActive(true);
        player.SetActive(false);
        povs = new GameObject[] {thirdPersonView, firstPersonView};
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {

            /*firstPersonActive = !firstPersonActive;*/
            cameraView = 1;

        }
    }

    private void LateUpdate()
    {

        Vector3 newPosition = Vector3.Lerp(transform.position, povs[cameraView].transform.position, Time.deltaTime * speed);
        Quaternion newRotation = Quaternion.Slerp(transform.rotation, povs[cameraView].transform.rotation, Time.deltaTime * speed);

        transform.position = newPosition;
        transform.rotation = newRotation;

        
    }

    public void CamViewChange()
    {
        cameraView = 1;
        StartCoroutine(AvatarSwitch());
    }

    IEnumerator AvatarSwitch()
    {
        yield return new WaitForSeconds(0.5f);
        PHIL.SetActive(false);
        player.SetActive(true);
    }
}
