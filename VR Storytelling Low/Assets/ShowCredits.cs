﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCredits : MonoBehaviour
{
    public GameObject player;
    public GameObject darkCube;
    public GameObject credits;
    public GameObject timeline;
    public GameObject leftController;
    // Start is called before the first frame update
    void Start()
    {
        darkCube.SetActive(false);
        credits.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void ShowEndExperienceCredits()
    {
        timeline.transform.parent = null;
        this.transform.parent = null;
        player.SetActive(false);
        leftController.SetActive(false);
        darkCube.SetActive(true);
        credits.SetActive(true);
    }
}
